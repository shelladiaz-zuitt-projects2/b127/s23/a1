/**Create single room */
// db.rooms.insertOne(
//     {
//         "name": "single",
//         "accomodate": 2,
//         "price": 1000,
//         "description": "A simple room with all the basic necessities"
//      }
// );


/**Additional */
// db.rooms.updateOne(
//     {
//         "_id": ObjectId("61360047c06e54a6ece1736e")
//     },
//     {
//         $set: {
//             "rooms_available": 10,
//             "isAvailable": false
//             }
//     }
// );


/**Insert Many */
// db.rooms.insertMany([
//     {
//         "name": "queen",
//         "accomodate": 4,
//         "price": 4000,
//         "description": "A room with a queen sized bed perfect for a simple getaway",
//         "rooms_available": 15,
//         "isAvailable": false
//      },
//      {
//         "name": "double",
//         "accomodate": 3,
//         "price": 2000,
//         "description": "A room fit for a small family going on a vacation",
//         "rooms_available": 5,
//         "isAvailable": false
//      }
// ]);

/**Find */
// db.rooms.find({ "name": "double" });

/**updateOne method queen rooms to 0 */
// db.rooms.updateOne(
//     {
//         "_id": ObjectId("61360533c06e54a6ece17370")
//     },
//     {
//         $set: {
//             "rooms_available": 0
//             }
//      }
//     );

/**Delete rooms 0 available */
// db.rooms.deleteMany({ "rooms_available": 0 });